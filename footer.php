<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after
 *
 * @package Root Under Boot
 */
?>

	</div><!-- #content -->
</div><!-- #page -->

<footer id="colophon" class="site-footer" role="contentinfo">
	<div class="site-info container text-muted">
		<?php do_action( 'rub_credits' ); ?>
		<a href="http://wordpress.org/" rel="generator"><?php printf( __( 'Proudly powered by %s', 'rub' ), 'WordPress' ); ?></a>
		<span class="sep"> | </span>
		<?php printf( __( 'Theme: %1$s by %2$s.', 'rub' ), 'Root Under Boot', '<a href="http://www.konekt.ee" rel="designer">Risto Niinemets</a>' ); ?>
	</div><!-- .site-info -->
</footer><!-- #colophon -->

<?php wp_footer(); ?>

</body>
</html>