<?php
/**
 * The template used for displaying page content in page.php
 *
 * @package Root Under Boot
 */
?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<header class="page-header">
		<h1 class="entry-title"><?php the_title(); ?></h1>
	</header><!-- .entry-header -->

	<div class="entry-content">
		<?php the_content(); ?>
		<?php
			wp_link_pages( array(
				'before'      => '<ul class="pagination">',
				'after'       => '</ul>'
			) );
		?>
	</div><!-- .entry-content -->
</article><!-- #post-## -->
