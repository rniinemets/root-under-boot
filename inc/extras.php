<?php
/**
 * Custom functions that act independently of the theme templates
 *
 * Eventually, some of the functionality here could be replaced by core features
 *
 * @package Root Under Boot
 */

/**
 * Get our wp_nav_menu() fallback, wp_page_menu(), to show a home link.
 */
function rub_page_menu_args( $args ) {
	$args['show_home'] = true;
	return $args;
}
add_filter( 'wp_page_menu_args', 'rub_page_menu_args' );

/**
 * Adds custom classes to the array of body classes.
 */
function rub_body_classes( $classes ) {
	// Adds a class of group-blog to blogs with more than 1 published author
	if ( is_multi_author() ) {
		$classes[] = 'group-blog';
	}
	// Adds a class when this is a front page
	elseif( is_front_page() ) {
		$classes[] = 'front-page';
	}

	return $classes;
}
add_filter( 'body_class', 'rub_body_classes' );

/**
 * Filter in a link to a content ID attribute for the next/previous image links on image attachment pages
 */
function rub_enhanced_image_navigation( $url, $id ) {
	if ( ! is_attachment() && ! wp_attachment_is_image( $id ) )
		return $url;

	$image = get_post( $id );
	if ( ! empty( $image->post_parent ) && $image->post_parent != $id )
		$url .= '#main';

	return $url;
}
add_filter( 'attachment_link', 'rub_enhanced_image_navigation', 10, 2 );


/**
 * Utility functions
 */
function add_filters( $tags, $function ) {
	foreach( $tags as $tag ) {
		add_filter($tag, $function);
	}
}

function is_element_empty( $element ) {
	$element = trim( $element );
	return empty( $element ) ? false : true;
}

function rub_wrap_pagination_links( $link, $i ) {
	global $page, $numpages, $multipage, $more;

	if ( $i == $page || ! $more && 1 == $page ) {
		return '<li class="active"><a href="#">' . $link . '</a></li>';
	}

	return '<li>' . $link . '</li>';
}
add_filter( 'wp_link_pages_link', 'rub_wrap_pagination_links', 10, 2 );
