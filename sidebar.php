<?php
/**
 * The Sidebar containing the main widget areas.
 *
 * @package Root Under Boot
 */
?>
	<div id="secondary" class="widget-area col-md-4" role="complementary">
		<?php do_action( 'before_sidebar' ); ?>
		
		<?php dynamic_sidebar( 'primary' ) ?>
	</div><!-- #secondary -->
