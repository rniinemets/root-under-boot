<?php echo get_avatar( $comment, $args['avatar_size'] ) ?>
<div id="comment-<?php comment_ID(); ?>" <?php comment_class( array( empty( $args['has_children'] ) ? '' : 'parent', 'media-body' ) ); ?>>
	<h4 class="comment-author vcard media-heading">
		<?php echo get_comment_author_link() ?>
	</h4><!-- .comment-author -->

	<time datetime="<?php echo comment_date( 'c' ); ?>">
		<a href="<?php echo htmlspecialchars( get_comment_link( $comment->comment_ID ) ) ?>"><?php printf( __( '%1$s', 'rub' ), get_comment_date(),  get_comment_time() ) ?></a>
	</time>
	<?php edit_comment_link(__('(Edit)', 'rub'), '', ''); ?>


	<?php if ( '0' == $comment->comment_approved ) : ?>
		<div class="comment-awaiting-moderation alert alert-info"><?php _e( 'Your comment is awaiting moderation.', 'rub' ); ?></div>
	<?php endif; ?>

	<?php comment_text(); ?>

	<?php
	comment_reply_link( array_merge( $args, array(
		'add_below' => 'div-comment',
		'depth'     => $depth,
		'max_depth' => $args['max_depth'],
		'before'    => '<div class="reply">',
		'after'     => '</div>',
	) ) );
	?>

	<hr>