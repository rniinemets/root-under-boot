<?php
/**
 * The template for displaying search forms in Root Under Boot
 *
 * @package Root Under Boot
 */
?>
<form role="search" method="get" class="search-form form-inline" action="<?php echo esc_url( home_url( '/' ) ); ?>">
	<div class="input-group">
		<input type="search" class="search-field form-control" placeholder="<?php echo _e( 'Search &hellip;', 'rub' ); ?>" value="<?php echo esc_attr( get_search_query() ); ?>" name="s">
		<span class="input-group-btn">
			<input type="submit" class="search-submit btn btn-primary" value="<?php echo _e( 'Search', 'rub' ); ?>">
		</span>
	</div>
</form>
