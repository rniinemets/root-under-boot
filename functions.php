<?php
/**
 * Root Under Boot functions and definitions
 *
 * @package Root Under Boot
 */


/**
 * Define helper constants
 */
$get_theme_name = explode( '/themes/', get_template_directory() );

define( 'RELATIVE_PLUGIN_PATH',  str_replace( home_url() . '/', '', plugins_url() ) );
define( 'RELATIVE_CONTENT_PATH', str_replace( home_url() . '/', '', content_url() ) );
define( 'THEME_NAME',            next( $get_theme_name ) );
define( 'THEME_PATH',            RELATIVE_CONTENT_PATH . '/themes/' . THEME_NAME );

/**
 * Add theme support
 */
add_theme_support( 'nice-search' );			  // Enable nice search URL
add_theme_support( 'bootstrap-top-navbar' );  // Enable Bootstrap's top navbar
add_theme_support( 'bootstrap-gallery' );     // Enable Bootstrap's thumbnails component on [gallery]
add_theme_support( 'woocommerce' );			  // Enable support for WooCommerce
add_theme_support( 'rewrites' );              // Enable URL rewrites
add_theme_support( 'root-relative-urls' );    // Enable relative URLs

/**
 * Set the content width based on the theme's design and stylesheet. 1140 is Bootstrap's default.
 */
if ( ! isset( $content_width ) )
	$content_width = 1140; /* pixels */

if ( ! function_exists( 'rub_setup' ) ) :
/**
 * Sets up theme defaults and registers support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which runs
 * before the init hook. The init hook is too late for some features, such as indicating
 * support post thumbnails.
 */
function rub_setup() {

	/**
	 * Make theme available for translation
	 * Translations can be filed in the /languages/ directory
	 * If you're building a theme based on Root Under Boot, use a find and replace
	 * to change 'rub' to the name of your theme in all the template files
	 */
	load_theme_textdomain( 'rub', get_template_directory() . '/languages' );

	/**
	 * This theme uses wp_nav_menu() in one location.
	 */
	register_nav_menus( array(
		'primary_navigation' => __( 'Primary Navigation', 'rub' ),
	) );

	// Add editor style
	add_editor_style( get_template_directory_uri() . '/assets/css/editor-style.css' );
}
endif; // rub_setup
add_action( 'after_setup_theme', 'rub_setup' );

function rub_register_sidebars() {
	/**
	* Creates a sidebar
	* @param string|array  Builds Sidebar based off of 'name' and 'id' values.
	*/
	$args = array(
		'name'          => __( 'Primary Sidebar', 'rub' ),
		'id'            => 'primary',
		'description'   => '',
		'class'         => '',
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h2 class="widgettitle">',
		'after_title'   => '</h2>'
	);

	register_sidebar( $args );
}
add_action( 'widgets_init', 'rub_register_sidebars' );

/**
 * Enqueue scripts and styles
 */
function rub_scripts() {
	wp_enqueue_style( 'rub-bootstrap', get_template_directory_uri() . '/assets/css/bootstrap.css' );
	wp_enqueue_style( 'rub-main', get_template_directory_uri() . '/assets/css/main.css' );
	wp_enqueue_style( 'rub-responsive', get_template_directory_uri() . '/assets/css/responsive.css' );

	if( current_theme_supports( 'woocommerce' ) )
		wp_enqueue_style( 'rub-woocommerce', get_template_directory_uri() . '/assets/css/woocommerce.css' );

	wp_enqueue_script( 'rub-javascript', get_template_directory_uri() . '/assets/js/main.js', array( 'jquery' ), false, true );
	wp_enqueue_script( 'rub-bootstrap', get_template_directory_uri() . '/assets/js/bootstrap.min.js', array( 'jquery' ), false, true );
}
add_action( 'wp_enqueue_scripts', 'rub_scripts' );

require get_template_directory() . '/inc/extras.php';
require get_template_directory() . '/inc/relative-urls.php';
require get_template_directory() . '/inc/comments.php';
require get_template_directory() . '/inc/rewrites.php';
require get_template_directory() . '/inc/activation.php';
require get_template_directory() . '/inc/nav.php';
require get_template_directory() . '/inc/clean-up.php';
require get_template_directory() . '/inc/gallery.php';
require get_template_directory() . '/inc/template-tags.php';

if( current_theme_supports( 'woocommerce' ) )
	require get_template_directory() . '/inc/woocommerce.php';

/* --------------------------------------------- *
 * 	OptionTree
 * --------------------------------------------- */
add_filter( 'ot_show_pages', '__return_true' );
add_filter( 'ot_show_new_layout', '__return_false' );
add_filter( 'ot_theme_mode', '__return_true' );

load_template( trailingslashit( get_template_directory() ) . 'option-tree/ot-loader.php' );
